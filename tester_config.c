#include "tester.h"

int main(int argc, char* argv[]) {
    
    char test_type;
    int line_skip = atoi(argv[1]);
    int test_parametrs[3];


    file_line_skip(line_skip+1);
    scanf("%c", &test_type);
    // Conf testów typu A
    if (test_type == 'A') {
        // test_parametrs[i]: [1] - minimalna liczba pracowników; [2] - maksymalna liczba pracowników; [3] - liczba sekcji
        if (scanf("%d %d %d", &test_parametrs[1], &test_parametrs[2], &test_parametrs[3]) != 3) {
            fprintf(stderr, "Podane parametry nie są liczbami całkowitymi.\n");
            return 1;
        }
        int numberOfCompanies[test_parametrs[3]];

        printf("%d\n", test_parametrs[3]);
        for (int i=0; i<test_parametrs[3]; i++) {
            if (scanf("%d", &numberOfCompanies[i]) != 1) {
                fprintf(stderr, "Podane parametry nie są liczbami całkowitymi.\n");
                return 1;
            }
            printf("%d %d %d\n", numberOfCompanies[i], test_parametrs[1], test_parametrs[2]);
        }
    }
    // Conf testów typu B
    else if (test_type == 'B') {
        // test_parametrs[i]: [1] - liczba firm w sekcji; [2] - liczba sekcji
        if (scanf("%d %d", &test_parametrs[1], &test_parametrs[2]) != 2) {
            fprintf(stderr, "Podane parametry nie są liczbami całkowitymi.\n");
            return 1;
        }
        int minEmployees[test_parametrs[2]], maxEmployees[test_parametrs[2]];

        printf("%d\n", test_parametrs[2]);
        for (int i=0; i<test_parametrs[2]; i++) {
            if (scanf("%d %d", &minEmployees[i], &maxEmployees[i]) != 2) {
                fprintf(stderr, "Podane parametry nie są liczbami całkowitymi.\n");
                return 1;
            }
            printf("%d %d %d\n", test_parametrs[2], minEmployees[i], maxEmployees[i]);
        }
    }
    
    else {
        fprintf(stderr, "Nie wczytano typu testu.");
        return 1;
    }

    return 0;
}