#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>

void file_line_skip(int lines_to_skip) {
  char c;
  for (int i=0; i<lines_to_skip; i++) {
      do {
        c = fgetc(stdin);
      } while (c != '\n');
  }
}

char times_read(char times[3][10]) {
  char c;
  for (int i = 0; i < 3; i++) {
    do {
      c = fgetc(stdin);
    } while (! isdigit(c) );
    scanf("%s", times[i]);
  }
  return c;
}