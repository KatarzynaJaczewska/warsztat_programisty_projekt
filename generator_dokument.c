#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#define max_syl_amount 1000
#define syl_len 5
#define syl_in_word 4
#define max_managers 4


// Generator losowych słów z sylab.
void random_words(int numberOfWords, int syllables_count, char syllables[max_syl_amount][syl_len + 1]) {
    for (int i = 0; i < ((rand() % numberOfWords) + 1); i++) {
        for (int j = 0; j < ((rand() % syl_in_word) + 1); j++) {
            int randomizedNumber = rand() % syllables_count;
            printf("%s", syllables[randomizedNumber]);
        }
        printf(" ");
    }
}

// Generator losowych managerów.
void random_managers(int numberOfManagers, int assignedWorkers[], int ManagerIDs[], int syllables_count, char syllables[max_syl_amount][syl_len + 1]) {
    printf("\n\\begin{center}\n{\\renewcommand{\\arraystretch}{1.5}\n\\renewcommand{\\tabcolsep}{0.2cm}\n");
    printf("\\begin{tabular} {|c|c|c|c|c|}\n\\hline\n\\multicolumn{5}{|c|}{\\large \\textbf{Managers}} \\\\\n\\hline\n");
    printf("Name & Surname & ID & Manager ID & Subordinates \\\\\n\\hline\n");
    for (int p = 0; p < (numberOfManagers + 1); p++) {
        random_words(1, syllables_count, syllables);
        printf("& ");
        random_words(1, syllables_count, syllables);
        if (p != 0) {
            printf("& %d & %d & %d \\\\\n\\hline\n", ManagerIDs[p], ManagerIDs[0], assignedWorkers[p-1]);
        }
        else {
            printf("& %d & & %d \\\\\n\\hline\n", ManagerIDs[0], numberOfManagers);
        }
    }
    printf("\\end{tabular}}\n\\end{center}");
}

// Generator losowych pracowników.
void random_employees(int numberOfManagers, int assignedWorkers[], int ManagerIDs[], int syllables_count, char syllables[max_syl_amount][syl_len + 1]) {
    printf("\n\\begin{center}\n{\\renewcommand{\\arraystretch}{1.5}\n\\renewcommand{\\tabcolsep}{0.2cm}\n");
    printf("\\begin{tabular} {|c|c|c|c|}\n\\hline\n\\multicolumn{4}{|c|}{\\large \\textbf{Workers}} \\\\\n\\hline\n");
    printf("Name & Surname & ID & Manager ID \\\\\n\\hline\n");
    for (int m = 1; m < numberOfManagers + 1; m++) {
        for (int p = 0; p < assignedWorkers[m-1]; p++) {
            random_words(1, syllables_count, syllables);
            printf("& ");
            random_words(1, syllables_count, syllables);
            int WorkID = (rand() % 900000) + 100000;
            printf("& %d & %d \\\\\n\\hline\n", WorkID, ManagerIDs[m]);
        }
    }
    printf("\\end{tabular}}\n\\end{center}");
}

// Główna funkcja
int main(int argc, char* argv[]) {

    int numberOfSections = 0;
    scanf("%d", &numberOfSections);
    
    // Sprawdzenie, czy podana liczba rozdziałów jest dodatnia.
    if (numberOfSections <= 0) {
        fprintf(stderr, "Liczba rozdziałów musi być dodatnią liczbą całkowitą.\n");
        return 1;
    }

    int numberOfCompanies[numberOfSections], minEmployees[numberOfSections], maxEmployees[numberOfSections];
    for (int i = 0; i < numberOfSections; i++) {
        numberOfCompanies[i], minEmployees[i], maxEmployees[i] = 0;
        if (scanf("%d %d %d", &numberOfCompanies[i], &minEmployees[i], &maxEmployees[i]) != 3) {
            fprintf(stderr, "Wystąpił jeden z dwóch problemów:\n1) Podane parametry nie są liczbami całkowitymi.\n2) Podana liczba rozdziałów nie zgadza się z liczbą parametrów.\n");
            return 1;
        }
        if (minEmployees[i] > maxEmployees[i]) {
            fprintf(stderr, "Podana minimalna liczba pracowników przewyższa maksymalną liczbę pracowników.\n");
            return 1;
        }
    }

    // Sprawdzenie, czy podane liczby są i czy są dodatnie.
    for (int i = 0; i < numberOfSections; i++) {
        if (numberOfCompanies[i] <= 0 && minEmployees[i] <= 0 && maxEmployees[i] <= 0) {
            fprintf(stderr, "Liczba firm, minimum i maksimum pracowników muszą być dodatnimi liczbami całkowitymi.\n");
            return 1;
        }
    }

    char syllables[max_syl_amount][syl_len + 1];
    int syllables_count;
    FILE *sylaby_path;
    
    if (!(sylaby_path = fopen("sylaby.txt", "r"))) {
        fprintf(stderr, "Plik 'sylaby.txt' nie istnieje lub znajduje się w innem folderze.\n");
        return 1;
    }
    
    if (fscanf(sylaby_path, "%d", &syllables_count) != 1) {
        fprintf(stderr, "Błąd podczas odczytu liczby sylab.\n");
        return 1;
    }

    // Sprawdzenie, czy podana liczba sylab jest nieujemna i mieści się w zakresie 1-1000.
    if (syllables_count <= 0 || syllables_count > 1000) {
        fprintf(stderr, "Liczba sylab musi być między 1 a 1000.\n");
        return 1;
    }

    for (int i = 0; i < syllables_count; i++) {
        if (fscanf(sylaby_path, "%s", syllables[i]) != 1) {
            fprintf(stderr, "Błąd podczas odczytu sylab.\n");
            return 1;
        }
    }
    fclose(sylaby_path);
    printf("\n");

    // Inicjalizacja generatora liczb losowych aktualnym czasem.
    srand((unsigned int)time(NULL));

    for (int i = 0; i < numberOfSections; i++) {
        printf("\\section{");
        random_words(2, syllables_count, syllables);
        printf("}\n");
        for (int j = 0; j < numberOfCompanies[i]; j++) {
            int numberOfEmployees = (rand() % (maxEmployees[i] - minEmployees[i])) + minEmployees[i];
            int numberOfManagers = (rand() % max_managers) + 1;
            if (numberOfManagers >= numberOfEmployees/2) {
                numberOfManagers = numberOfManagers/2;
            }
            int commonWorkers = numberOfEmployees - (1 + numberOfManagers);
            int ManagerID[numberOfManagers + 1];
            int assignedWorkers[numberOfManagers];
            for (int k = 0; k < numberOfManagers; k++) {
                assignedWorkers[k] = 1;
                if (commonWorkers <= 0) {
                    numberOfManagers = k+1;
                    break;
                } else {
                    ManagerID[k+1] = 0;
                    ManagerID[k+1] = (rand() % 900000) + 100000;
                    if (k == numberOfManagers-1 || numberOfManagers == 1) {
                        assignedWorkers[k] = commonWorkers;
                    } else {
                        assignedWorkers[k] = (rand() % commonWorkers);
                        if (assignedWorkers[k] == 0) {
                            assignedWorkers[k] += 1;
                        }
                        commonWorkers = commonWorkers - assignedWorkers[k];
                    }
                }
            }

            ManagerID[0] = (rand() % 900000) + 100000;  // ID szefa (managera głównego)
            printf("\\begin{center}\n\\Large \\textbf{");
            random_words(3, syllables_count, syllables);
            printf("\n}\n");
            printf("\\end{center}\n");
            random_words(250, syllables_count, syllables);
            random_managers(numberOfManagers, assignedWorkers, ManagerID, syllables_count, syllables);
            random_employees(numberOfManagers, assignedWorkers, ManagerID, syllables_count, syllables);
            printf("\\pagebreak\n");
        }
    }
    return 0;
}
