#include "tester.h"

#define info_amount 7

int main(int argc, char* argv[]) {
    
    int numberOfSections = atoi(argv[1]);
    int i = 0;
    const char time_types[3][6] = {"real", "user", "sys"};
    char times[3][10];
    char info[info_amount][256];
    FILE* info_file = fopen("info.txt", "r");
    while (fgets(info[i], sizeof(info[i]), info_file) && i < info_amount) {
        if (info[i][strlen(info[i])-1] == '\n') {
            info[i][strlen(info[i])-1] = '\0';    // Removing the "new line" in string from words.txt
        }
        i++;
    }
    fclose(info_file);
    
    printf("\\documentclass{article}\n");
    printf("\\usepackage{underscore}\n");
    printf("\\begin{document}\n");
    printf("\\title{Raport Testera}\n");
    printf("\\maketitle\n");
    printf("DANE:\\\\\n");
    printf("Czas uruchomienia testera: %s\\\\\n", info[0]);
    printf("Czas ukonczenia dzialania testera: %s\\\\\n", info[1]);
    printf("Nazwa pliku konfiguracyjnego: %s\\\\\n", info[2]);
    printf("Ilosc testow: %s\\\\\n", info[3]);
    printf("Lokalizacja wygenerowanych dokumentow: %s\\\\\n", info[4]);
    printf("Lokalizacja logow: %s\\\\\n", info[5]);
    printf("Lokalizacja testowanego generatora: %s\\\\\n", info[6]);

    for (int i = 0; i < numberOfSections; i++) {
        char scanned_char = times_read(times);
        printf("\\section{Wyniki Testu %d}\n", i+1);
        printf("\\begin{center}\n{\\renewcommand{\\arraystretch}{1.5}\n\\renewcommand{\\tabcolsep}{0.2cm}\n");
        printf("\\begin{tabular} {|c|c|}\n\\hline\n\\multicolumn{2}{|c|}{\\large \\textbf{Wyniki pomiaru czasu}} \\\\\n\\hline\n");
        printf("Typ czasu & Czas \\\\\n\\hline\n");
        for (int r = 0; r < 3; r++) {
            printf("%s & %c%s \\\\\n\\hline\n", time_types[r], scanned_char, times[r]);
        }
        printf("\\end{tabular}}\n\\end{center}");
    }

    printf("\\end{document}");
    return 0;
}