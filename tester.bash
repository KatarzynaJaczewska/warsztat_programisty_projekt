#!/bin/bash

set -e # flaga, która zabiwa bash-script jeżeli bład występuje gdzieś indziej

if [ "$#" -ne 1 ]; then
    echo "Użycie: $0 <nazwa_pliku_konfiguracyjnego>"
    exit 1
fi

if ! [[ "$1" =~ ^.*\.conf$ ]]; then
    conf_file="$1.conf"
else
    conf_file="$1"
fi

if [ ! -f "$conf_file" ]; then
    echo "Konfiguracyjny plik z taką nazwą nie istnieje lub znajduje się w innem folderze."
    exit 1
fi

if [ ! -f "moj_tester1" ]; then
    echo "Program 'moj_tester1' nie istnieje lub nie jest wykonywalny."
    exit 1
fi
if [ ! -f "moj_tester2" ]; then
    echo "Program 'moj_tester2' nie istnieje lub nie jest wykonywalny."
    exit 1
fi

if [ ! -d "dokumenty" ]; then
    mkdir dokumenty
fi
if [ ! -d "log" ]; then
    mkdir log
fi

ilosc_testow=$(sed -n '1{p;q}' $conf_file)
if [[ ! $ilosc_testow =~ ^[0-9]+$ ]]; then
   echo "Liczba testów musi być dodatnią liczbą całkowitą."
   exit 1
fi

raport="raport"
lokalizacja_logow="log"
lokalizacja_generatora="./"
lokalizacja_dokumentow="dokumenty"
date '+%Hh %Mm %S.%3Ns' > info.txt
echo "" > time.txt  # utworzenie nowego/wyczyszczenie starego pliku time.txt
echo "" > $folder_logow/generator.log # utworzenie nowego/wyczyszczenie starego pliku generator.log

for ((i = 0; i < $ilosc_testow; i++)) do
    numer_dokumentu=$(($i + 1))
    numer_konfiguracji=$(($i + 1))
    linii_do_opuszczenia=$(($i * 2))

    ./moj_tester1 $linii_do_opuszczenia  < $conf_file > "generator_$numer_konfiguracji.conf"
    (time ./generator.bash "generator_$numer_konfiguracji.conf" "dokument_$numer_dokumentu.tex" >> $folder_logow/generator.log) 2>> time.txt

    mv generator_$numer_konfiguracji.conf $lokalizacja_logow
    mv dokument_$numer_dokumentu.aux $lokalizacja_logow
    mv dokument_$numer_dokumentu.log $lokalizacja_logow
    mv dokument_$numer_dokumentu.* $lokalizacja_dokumentow
done

date '+%Hh %Mm %S.%3Ns' >> info.txt
echo $conf_file >> info.txt
echo $ilosc_testow >> info.txt
echo $(realpath $lokalizacja_dokumentow) >> info.txt
echo $(realpath $lokalizacja_logow) >> info.txt
echo $(realpath generator.bash) >> info.txt
./moj_tester2 $ilosc_testow < time.txt > "$raport.tex"
pdflatex "$raport.tex" > $folder_logow/tester.log
mv $raport.aux $lokalizacja_logow
mv $raport.log $lokalizacja_logow


if [ $? -ne 0 ]; then
    echo "Błąd podczas kompilacji pliku .tex."
    exit 1
fi

echo "Dokument został pomyślnie utworzony i skompilowany do formatu PDF."
exit 0