
#!/bin/bash

set -e # flaga, która zabiwa bash-script jeżeli bład występuje w .C

if [ "$#" -ne 2 ]; then
    echo "Użycie: $0 <nazwa_pliku_konfiguracyjnego> <pożądana_nazwa_pliku_pdf>"
    exit 1
fi


if ! [[ "$1" =~ ^.*\.conf$ ]]; then
    conf_file="$1.conf"
else
    conf_file="$1"
fi

if [ ! -f "$conf_file" ]; then
    echo "Konfiguracyjny plik z taką nazwą nie istnieje lub znajduje się w innem folderze."
    exit 1
fi

if [ ! -f "moj_generator" ]; then
    echo "Program 'moj_generator' nie istnieje lub nie jest wykonywalny."
    exit 1
fi


if ! [[ "$2" =~ ^.*\.tex$ ]]; then
    tex_file="$2.tex"
else
    tex_file="$2"
fi

echo "\\documentclass{article}" > "$tex_file"
echo "\\begin{document}" >> "$tex_file"

echo "\\title{Lista firm}" >> "$tex_file"
echo "\\maketitle" >> "$tex_file"
# Generowanie i wypisywanie rozdziałów poprzez program w języku C
./moj_generator < "$conf_file" >> "$tex_file"

echo "\\end{document}" >> "$tex_file"


if [ ! -f "$tex_file" ]; then
    echo "Błąd podczas tworzenia pliku .tex."
    exit 1
fi

pdflatex "$tex_file"

if [ $? -ne 0 ]; then
    echo "Błąd podczas kompilacji pliku .tex."
    exit 1
fi

echo "Dokument został pomyślnie utworzony i skompilowany do formatu PDF."

exit 0

